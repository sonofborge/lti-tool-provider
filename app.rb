#
# LTI Tool Provider App
#

# Gem Dependencies
require 'ims/lti'
require 'oauth/request_proxy/rack_request' # must include the oauth proxy object
require 'sinatra'

# Enable Sessions
enable :sessions
set :protection, :except => :frame_options

# Consumer keys/secrets
$oauth_creds = {"consumerKey" => "consumerSecret"}

# Show Error Message
def show_error(message)
  @message = message
end

# Verify the OAuth Signature
def authorize!
  if key = params['oauth_consumer_key']
    if secret = $oauth_creds[key]
      @tool_provider = IMS::LTI::ToolProvider.new(key, secret, params)
    else
      @tool_provider = IMS::LTI::ToolProvider.new(nil, nil, params)
      @tool_provider.lti_msg = "Your consumer didn't use a recognized key."
      @tool_provider.lti_errorlog = "Unrecognized key. Please try again."
      show_error "Consumer key wasn't recognized"
      return false
    end
  else
    show_error "No consumer key"
    return false
  end

  if !@tool_provider.valid_request?(request)
    show_error "The OAuth signature was invalid"
    return false
  end

  if Time.now.utc.to_i - @tool_provider.oauth_timestamp.to_i > 60*60
    show_error "Your request is too old."
    return false
  end

  if was_nonce_used_in_last_x_minutes?(@tool_provider.request_oauth_nonce, 60)
    show_error "Why are you reusing the nonce?"
    return false
  end

  @username = @tool_provider.username
  return true
end

def was_nonce_used_in_last_x_minutes?(nonce, minutes = 60)
  # some kind of caching solution or something to keep a short-term memory of used nonces
  false
end

# Routes
require './routes.rb'
